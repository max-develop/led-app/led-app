module gitlab.com/baroprime/led-app/led-app

go 1.12

require (
	github.com/alexellis/blinkt_go v0.0.0-20180120180744-cc0ca163e0bc // indirect
	github.com/gorilla/mux v1.7.2 // indirect
)
