# Docs for the led-app

led-app contains all files to run the app


## build

1) build the device-blinkt on the device (in this case on the rpi zero).
2) run the Makefile

### redis-api


|Method|URI|Description|
|------|---|-----------|
|POST|`/api/create-device`|Creates a new device|
|GET|`/api/get-device/{id}`|Returns device with specifid id. * for all|
|PUT|`/api/update-device`|Update device|
|DELETE|`/api/delete-device/{id}`|Delete device|

#### `/api/create-device`

Format example:

```javascript
{
  "ID": "1",
  "Name": "ayy",
  "DeviceType": "blinkt",
  "Values": {
    "R": "255",
    "G": "0",
    "B": "0",
    "A": "0.1" //0.1-1
  }
}
```
#### `/api/get-device/{id}`

Format example:

 - `http://url:port/api/get-device/1` Get device with ID 1

 - `http://url:port/api/get-device/*` Get all devices

#### `/api/update-device`

Format example:

```javascript
{
  "ID": "1",
  "Name": "ayy",
  "DeviceType": "blinkt",
  "Values": {
    "R": "255",
    "G": "0",
    "B": "0",
    "A": "0.1" //0.1-1
  }
}
```
#### `/api/delete-device/{id}`

Format example:

 - `http://url:port/api/delete-device/1` Delete device with ID 1