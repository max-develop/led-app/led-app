default: all

SERVICES ?= device-api frontend-api redis-api

# ----------- Aggregators -------------------
all: build upload
build: build-device-blinkt build-all
upload: push


# ------------- builds ----------------------
build-all:
	@echo "Building images"
	@for service in $(SERVICES); do \
		docker image build \
		-t baroprime/$$service:arm \
		../$$service/. ; \
	done

build-device-blinkt:
	@echo "Building device-blinkt"
	@GOOS=linux GOARCH=arm GOARM=6 go build -o ../device-blinkt/blinkt ../device-blinkt/*.go



# ------------- uploads ----------------------
push:
	@echo "Pushing images"
	@for service in $(SERVICES); do \
		docker image push \
		baroprime/$$service:arm ; \
	done


